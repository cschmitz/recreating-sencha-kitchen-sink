Ext.define('SmallDemos.model.Order', {
    extend: 'SmallDemos.model.Base',
    
    fields: [
        { name: 'date', type: 'date', dateFormat: 'Y-m-d' },
        { name: 'shipped', type: 'string' },
        { 
            name: 'customerId', 
            reference: {
                parent: 'Customer'
            }
        }
    ],

    proxy:{
        type: 'rest',
        url: 'KitchenSink/Order'
    }


});
