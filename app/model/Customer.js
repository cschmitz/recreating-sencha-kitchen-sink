Ext.define('SmallDemos.model.Customer', {
    extend: 'SmallDemos.model.Base',
    
    fields: [
        { name: 'id', type: 'integer'},
        { name: 'name', type: 'string' },
        { name: 'phone', type: 'string' }
    ],

    proxy: {
        type: 'rest',
        url: '/SmallDemos/Customer',
        reader:{
            type: 'json',
            // rootProperty: 'customers'
        }
    },

    validators: {
        name: 'presence'
    }
});
