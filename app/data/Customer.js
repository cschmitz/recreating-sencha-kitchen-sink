Ext.define('SmallDemos.data.Customer', {
    requires: [
        'SmallDemos.data.Init'
    ]
}, function (){
        var customers = [
            {id: 1, name: 'Chris', phone: '123.456.7890'},
            {id: 2, name: 'Ruthie', phone: '098.765.4321'},
            {id: 3, name: 'Zoe', phone: '111.222.3333'},
            {id: 4, name: 'Petey', phone: '444.555.6666'},
            {id: 5, name: 'Madori', phone: '777.888.9999'}
        ];
        Ext.ux.ajax.SimManager.register({
            type: 'json',
            // Look for the url /SmallDemos/Customer with an optional pattern "/ followed by a number" and ending with a forward slash
            url: /\/SmallDemos\/Customer(\/\d+)?/,
            data: function (ctx){
                // I'm assuming we *need* the parenthesis in the url regex so that we can use the [1] below to grab the first thing the regex was told to "remember" but I'm not sure
                var idPart = ctx.url.match(this.url)[1];
                var id;

                if(idPart){
                    id = parseInt(idPart.substring(1), 10);
                    return Ext.Array.findBy(cusotmers, function (customer){
                        return customer.id === id;
                    });
                } else {
                    return customers;
                }
            }
        });
});