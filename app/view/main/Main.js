/**
 * This class is the main view for the application. It is specified in app.js as the
 * "autoCreateViewport" property. That setting automatically applies the "viewport"
 * plugin to promote that instance of this class to the body element.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('SmallDemos.view.main.Main', {
    extend: 'Ext.container.Container',
    requires: [
        'SmallDemos.view.main.MainController',
        'SmallDemos.view.main.MainModel',
        'SmallDemos.view.binding.HelloWorldTab',
        'SmallDemos.view.binding.DynamicBinding',
        'SmallDemos.view.binding.TwoWayBinding',
        'SmallDemos.view.binding.Formulas',
        'SmallDemos.view.binding.associations.Associations',
        'SmallDemos.view.binding.ComponentState',
        'SmallDemos.view.binding.ModelValidation',
        'SmallDemos.view.panels.PanelExamples',
        'SmallDemos.view.panels.HeaderPosition'
    ],

    layout: {
        type: 'fit'
    },

    items: [
        {
            xtype: 'panel',
            layout: {
                type: 'accordion'
            },

            items:[
                {
                    xtype: 'tabpanel',
                    title: 'Panel Examples',
                    items:[
                        {
                            xtype: 'headerposition'
                        },
                        {
                            xtype: 'panelExamples'
                        }
                    ]
                },
                {
                    xtype: 'tabpanel',
                    title: 'Data Binding Examples',
                    items:[
                        {
                            xtype: 'ModelValidation'
                        },
                        {
                            xtype: 'componentState'
                        },
                        {
                            xtype: 'helloWorld'
                        },
                        {
                            xtype: 'dynamicBinding'
                        },
                        {
                            xtype: 'twoWayBinding'
                        },
                        {
                            xtype: 'formulas'
                        },
                        {
                            xtype: 'associations'
                        }
                    ]
                },
                {
                    xtype: 'tabpanel',
                    title: 'Layout Examples',
                    items:[
                        {
                            xtype: 'absoluteLayout'
                        }
                    ]
                }
            ]
        }
    ]
});
