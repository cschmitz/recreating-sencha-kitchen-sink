Ext.define('SmallDemos.view.layouts.AbsoluteController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.layouts-absolute',

    getPanelPosition: function (){
        debugger;
        var panelposition = this.lookupReference('panel1').getPosition();
        var viewmodeldata = this.getViewModel().getData();
        var vmPanel1x     = viewmodeldata.panel1.x;
        var vmPanel1y     = viewmodeldata.panel1.y;
        console.log('panel position: ' + panelposition);
        console.log('viewmodel data: ' + vmPanel1x + ", " + vmPanel1y);
    },

    resetPanelPosition: function (){
        debugger;
        var viewmodel = this.getViewModel();
        // It should be noted and is ULTRA IMPORTANT to understand that the value we set here MUST NOT match the 
        // current value in the viewmodel or the panel will not be moved. Theoretically these values should never
        // be out of sync with the viewmodel's values, but for some reason in this kitchen sink re-creation the panel
        // does not start in the correct position. I suspect it has something do do with the fact that the `Absolute`
        // view is not the first to be displayed and therefore the panel rendering and the viewmodel instantiation 
        // happen in a different order, but I feel like digging in and figuring out exactly what's happening is beyond
        // the scope of what I'm trying to do with this project. 
        viewmodel.set('panel1.x', 300);
        viewmodel.set('panel1.y', 300);
    },


    // `move` (the original name of this method) is the name of an existing
    // method in the `Positionable` utility class, so we can't use it.
    doMove: function (owner, tool, event){
        debugger;

        // Note that these were left in when the sencha forum user was rewriting the code, but on review
        // of the changes I notice they're not used at all. The only reason I'm leaving them in here commented
        // out is to help drive home that the actual move of the panel has **absolutely nothing to do with the viewmodel data**.
        // var viewmode = this.getViewModel();
        // panel1 = viewmodel.data.panel1,
        // panel2 = viewmodel.data.panel2;

        var panel = tool.up('panel');
        var stepSize = 5;
        switch(tool.type){
            case 'left':
                // note that we're using setPagePosition instead of setPosition
                // setPagePosition sets the actual x and y for the absolute 
                // positioning. setPosition just sets the top and left coordinates
                panel.setPagePosition( panel.getX() - stepSize ); 
                break;
            case 'right':
                // we're also using `getX()` and `getY()` methods  rather than
                // accessing the x and y properties directly (e.g. panel.x) because
                // the methods are the purpose built getters for the position in the dom. 
                panel.setPagePosition( panel.getX() + stepSize ); 
                break;
            case 'up':
                panel.setPagePosition( panel.getX() , panel.getY() - stepSize ); 
                break;
            case 'down':
                panel.setPagePosition( panel.getX() , panel.getY() + stepSize ); 
                break;
        }

    }
    
});
