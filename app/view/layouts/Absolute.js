Ext.define('Movable',{
    // override overrides members of a specific class
    // NOTE: it looks like when you override a class, it overrides EVERY instance of the class
    // If you open the javascript tools you'll see the 'init' text in the console.log below 
    // printed out the same number of times as the count of ALL of the panels in your app/
    override: 'Ext.panel.Panel',

    initEvents: function (){
        this.callParent();
        console.log('init');
        this.on('move', function (){
            // publishState adds the data to the ViewModel under the given property name, in this case, 'x' and 'y'.
            // 
            // It is *VERY* important to note that this publishing has nothing to do with the **movement** of the panel,
            // It's only there to facilitate the updating of the html within the panel. You could hard code the initial 
            // x and y for the panel and move those configs outside of the `bind` property and the panel will still *move*.
            // The move is happening because in the `doMove` method of the AbsoluteController we are setting the page position
            // for the panel (not the viewmodel data) using the current x or y *config* value (not the viewmodel data). 
            // these publishStates that happen when the move event is fired just make sure that the viewmodel data matches 
            // the current position of the panel. 
            this.publishState('x', this.getX());
            this.publishState('y', this.getY());
        }, this);
    }
});

Ext.define("SmallDemos.view.layouts.Absolute",{
    extend: "Ext.panel.Panel",

    xtype:'absoluteLayout',

    title: 'Absolute Layout',

    // Quoted from sencha forum reply: https://www.sencha.com/forum/showthread.php?302071-ViewModel-data-not-binding-to-panel-s-x-and-y-configs&p=1104234&viewfull=1#post1104234
    // "Use "uses" instead of "requires" whenever possible. 
    // Requires forces the loader to find the class before the definition. 
    // Uses forces the loader to find the class before instantiation. 
    // You almost never need to use Requires and it breaks fiddle since the code is inlined."
    //
    // So, we use uses instead of requires because requires means that the order in which classes are read by the framework matters
    // and uses means all classes are all loaded so when instantiating this class use these given dependencies. (I'm pretty sure that's what that means)
    uses: [
        "SmallDemos.view.layouts.AbsoluteController",
        // "SmallDemos.view.layouts.AbsoluteModel",
        'Ext.layout.container.Absolute'
    ],

    controller: "layouts-absolute",

    //======================================================//
    // Note that this is NOT the way it should be done, it's just a hacky way I was able to get the panel to show
    // on screen in the demo. Ideally I would dig in and figure out why the panel won't show on screen at the 
    // begining, but I'd like to move on in this kitchen sing recreation.
    initComponent: function (){
        this.viewModel =  {
            data:{
                panel1:{
                    x: 301,
                    y: 301
                }
            }
        };
        this.updateLayout();
        this.callParent();
    },
    viewModel: true,
    //======================================================//

    // `publishes` notes config options that should be published to the viewModel
    publishes: {
        x: true,
        y: true
    },
    
    width: 500,
    height: 400,

    defaults: {
        bodyPadding: 15,
        width: 300, 
        height: 100,
        frame: true,
        xtype: 'panel'
    },

    items:[
        {
            xtype: 'button', text: 'get panel position', width:150, height:50, handler: 'getPanelPosition'
        },
        {
            xtype: 'button', text: 'reset panel position', width:150, height:50, handler: 'resetPanelPosition'
        },
        {
            title: 'Panel 1',
            reference: 'panel1',
            bind:{
                // Note that the x and y still need to be bound to the viewModel's data so that we can update the viewmodel's data

                // Also note that even though the only thing we actually *see* is the html config here, we still need to bind the 
                // x and y config of the panel so that when our override sets the state of the panel's x and y the bound text in 
                // the html is updated along with it, i.e. if we removed the x and y configs from the binding, the html would never
                // update because the x and y of the viewmodel would never have a reason to update. 
                // I suspect we may be able to remove the need to bind the x any y properties if we altered the methods in the viewcontroller
                // to just update the viewmodel data directly during the move (an thereby removing the reliance on the updated state of the x and y),
                // but I'm mentally tired out just from typing this annotation so I'm not going to try it out. 


                x: '{panel1.x}',
                y: '{panel1.y}',
                html: 'This is positioned at x: {panel1.x} , y: {panel1.y}',
            },
            tools:[
                { type: 'left', callback: 'doMove'},
                { type: 'right', callback: 'doMove'},
                { type: 'up', callback: 'doMove'},
                { type: 'down', callback: 'doMove'}
            ],
            // afterRender: function (){
            //     debugger;
            //     var controller = this.lookupController();
            //     controller.resetPanelPosition();
            // }
        }
    ]
});
