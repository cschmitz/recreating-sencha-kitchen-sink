
Ext.define("SmallDemos.view.binding.TwoWayBinding",{
    extend: "Ext.panel.Panel",

    xtype: 'twoWayBinding',

    viewModel:{
        data: {
            title: 'Change me!'
        }
    },

    bind:{
        title: '{title}'
    },

    items:[
        {
            xtype: 'textfield',
            fieldLabel: 'Set the title',
            bind:{
                value: '{title}'
            }
        }
    ]

});
