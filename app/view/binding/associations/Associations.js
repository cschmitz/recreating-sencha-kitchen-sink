
Ext.define("SmallDemos.view.binding.associations.Associations",{
    extend: "Ext.panel.Panel",

    requires: [
        'SmallDemos.model.Customer'
    ],

    viewModel:{
        stores: {
            customers:{
                model: 'Customer', 
                autoLoad: true,
            }
        }
    },

    xtype: 'associations',
    title: 'Associations',

    referenceHolder: true, 
    // layout: 'hbox',
    layout: 'vbox',
    session: {},

    items:[
        {
            xtype: 'button',
            text: 'debug',
            handler: function (){

                var x = 1;
            }
        },
        {
            xtype: 'textarea',
            width: 400,
            height: 400,
            bind: {
                value: '{customers}'
            }
        },
        {
            title: "All Customers",
            xtype: 'grid',
            bind: '{customers}',
            reference: 'customerGrid',
            flex: 1,
            columns: [
                {text: 'Name', dataIndex: 'name', flex: 1},
                {text: 'Phone', dataIndex: 'phone'}
            ]

        }
    ]

});
