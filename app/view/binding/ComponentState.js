
Ext.define("SmallDemos.view.binding.ComponentState",{
    extend: "Ext.panel.Panel",

    alias: 'widget.componentState',

    title: 'Component State',

    viewModel: true, 

    items:[
        {
            xtype: 'checkbox',
            boxLabel: 'Is Admin',
            reference: 'isAdmin'
        },
        {
            xtype: 'textfield',
            fieldLabel: 'Admin Key',
            bind: {
                disabled: '{!isAdmin.checked}'
            }
        }
    ]
});
