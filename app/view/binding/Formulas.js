
Ext.define("SmallDemos.view.binding.Formulas",{
    extend: "Ext.panel.Panel",
    alias: 'widget.formulas',

    requires: [
        "SmallDemos.view.binding.FormulasModel"
    ],

    title: 'formulas!',
    bodyStyle: {
        background: '#EDFAD5'
    },

    viewModel: {
        type: "binding-formulas",
        data:{
            x: 10,
            y: 50,
            name: {
                first: 'Chris',
                last: 'Schmitz'
            }
        }
    },

    layout:{
        type: 'vbox',
        pack: 'center',
        align: 'middle'
    },
    defaults:{
        width: 600,
        border: true,
        bodyPadding: 10
    },
    items:[
        {
            xtype: 'panel',
            title: 'Fields',
            items:[
                {
                    xtype: 'numberfield',
                    fieldLabel: 'Number X', 
                    bind: '{x}'
                },
                {
                    xtype: 'numberfield',
                    fieldLabel: 'Number Y', 
                    bind: '{y}'
                },
                {
                    xtype: 'textfield',
                    fieldLabel: 'First Name',
                    bind: '{name.first}'
                },
                {
                    xtype: 'textfield',
                    fieldLabel: 'Last Name',
                    bind: '{name.last}'
                }
            ]
        },
        {
            xtype: 'panel',
            title: 'Calcs',
            defaults:{
                xtype: 'displayfield',
                labelAlign: 'right',
            },
            items: [
                {
                    fieldLabel: 'Calculated', 
                    bind: '{x} * 2 = {twice} / {x} * 4 = {quad} {y}'
                },
                {
                    fieldLabel: 'X and Y combined',
                    bind: '{x} + {y} = {combine}'
                },
                {
                    fieldLabel: 'Just Y',
                    bind: '{justy}'
                },
                {
                    fieldLabel: 'Full Name (First Last)',
                    bind: '{fullNameFirstLast}'
                },
                {
                    fieldLabel: 'Full Name (Last, First)',
                    bind: '{fullNameLastFirst}'
                },
                {
                    fieldLabel: 'Name Mirror',
                    bind: '{nameMirror}'
                }
            ]
        }
    ]

});
