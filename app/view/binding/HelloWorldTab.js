
Ext.define("SmallDemos.view.binding.HelloWorldTab",{
    extend: "Ext.panel.Panel",


    xtype: 'helloWorld',

    viewModel: {
        data: {
            title: "Hello World Panel",
            html: 'Why hello, world :)'
        }
    },
    bind:{
        title: '{title}',
        html: '{html}'
    }
});
