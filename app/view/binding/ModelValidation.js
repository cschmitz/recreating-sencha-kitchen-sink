
Ext.define("SmallDemos.view.binding.ModelValidation",{
    extend: "Ext.panel.Panel",

    xtype: 'ModelValidation',

    title: 'Model Validation',

    modelValidation: true, 

    session: true, 
    // viewModel:{
    //     links: {
    //         theCustomer: {
    //             type: 'Customer',
    //             id: 1
    //         }
    //     }
    // },
    viewModel:{
        data:{
            theCustomer: 
            {
                id: 2, name: ""
            }
        }
    },
    items:[
        {
            xtype: 'textfield',
            fieldLabel: 'Name',
            msgTarget: 'slide',
            bind: '{theCustomer.name}'
        }
    ]

});
