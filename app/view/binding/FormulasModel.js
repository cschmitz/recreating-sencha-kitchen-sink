Ext.define('SmallDemos.view.binding.FormulasModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.binding-formulas',
    data: {
        name: 'SmallDemos'
    },

    formulas:{

        // the `get` argument is an *arbitrary* name (you can change `get` to `lol and
        // it would still work) we give to a function that sencha
        // passes in that allows you to get the data values for the data properties
        // THAT YOU'VE USED IN THE FUNCTION. i.e. if you try `get('y')` in the quad
        // function you'll get `undefined` because `y` isn't used anywhere in your 
        // function.
        quad: function (get){
            return get('twice') * 2;
        },

        twice:{
            get: function (get){
                var y = get('y');
                return get('x') * 2;
            }

        },

        combine: function (get){
            return get('x') + get('y');
        },

        justy:{
            bind: '{y}',
            get: function (y){
                return "just " + y;
            }
        },

        fullNameFirstLast: function (get){
            return get('name.first') + " " + get('name.last');
        },

        fullNameLastFirst: function (get){
            return get('name.last') + ", " + get('name.first');
        },

        nameMirror: function (get){
            var fullname = get('fullNameFirstLast');
            var reverse = this.reverseString(fullname);
             return fullname.toLowerCase() + " | " + reverse.toLowerCase();
        }
    },

    reverseString: function (string){
        return string.split("").reverse().join("");
    }

});
