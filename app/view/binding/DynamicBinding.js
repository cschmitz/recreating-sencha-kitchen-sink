
Ext.define("SmallDemos.view.binding.DynamicBinding",{
    extend: "Ext.panel.Panel",

    requires: [
        "SmallDemos.view.binding.DynamicBindingController",
        "SmallDemos.view.binding.DynamicBindingModel"
    ],

    xtype: 'dynamicBinding',

    controller: 'binding-dynamicbinding',

    viewModel: {
        data: {
            title: 'My Title',
            content: 'gibberish'
        }
    },

    bind:{
        title: 'Title: {title}',
        html: 'Content: {content}'
    },

    tbar:[
        {
            text: 'Change Title',
            listeners:{
                click: 'onChangeTitleClick'
            }
        },
        {
            text: 'Change Content',
            listeners:{
                click: 'onChangeContentClick'
            }
        }
    ]


});
