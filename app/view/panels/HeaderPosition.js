
Ext.define("SmallDemos.view.panels.HeaderPosition",{
    extend: "Ext.panel.Panel",
    xtype: 'headerposition',


    viewModel: true,

    title: 'Header Position',

    width: 600,
    layout: 'column',
    defaults: {
        bodyPadding: 10,
        height: 300,
        autoScroll: true
    },

    initComponent: function (){

        this.bodyStyle = "background: transparent";

        this.tbar = [
            {
                xtype: 'label',
                text: 'Header Position'
            },
            {
                xtype: 'segmentedbutton',
                reference: 'positionBtn',
                value: 'top',
                defaultUI: 'default',
                items: [
                    {
                        text: 'Top',
                        value: 'top'
                    },
                    {
                        text: 'Right',
                        value: 'right'
                    },
                    {
                        text: 'Left',
                        value: 'left'
                    },
                    {
                        text: 'Bottom',
                        value: 'bottom'
                    }
                ]
            }
        ];

        this.items = [
            {
                columnWidth: 0.5,
                margin: '10 5 0 0',
                title: 'Panel',
                icon: null,
                glyph: 117,
                html: SmallDemos.DummyText.longText,
                bind: {
                    headerPosition: '{positionBtn.value}',
                    
                }
            },
            {
                columnWidth: 0.5,
                margin: '10 5 0 0', 
                title: 'Framed Panel',
                frame: true,
                icon: null,
                glyph: 117,
                html: SmallDemos.DummyText.longText,
                bind: {
                    headerPosition: '{positionBtn.value}'
                }
            }
        ];

        this.callParent();
    }
});
