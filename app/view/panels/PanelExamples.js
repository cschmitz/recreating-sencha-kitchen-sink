Ext.define("SmallDemos.view.panels.PanelExamples",{
    extend: "Ext.Container",

    xtype: 'panelExamples',
    title: 'Panels Example',

    width: 660,

    requires:[
        'Ext.layout.container.Table'
    ],

    layout:{
        type: 'table',
        columns: 3,
        tdAttrs: { style: 'padding: 10px; vertical-align: top'}
    },

    defaults: {
        xtype: 'panel',
        width: 200,
        height: 280,
        bodyPadding: 10,
        frame: true
    },

    // Note that you can build this call either using initComponent like this or 
    // with the regular `items` property in the commented out section below, but
    // because there are dependencies involved (i.e. the DummyText class) we 
    // *should* use initComponet because it makes sure that the class we're 
    // depending on has been initialized *before* this class is initialized.
    // It may not seem to make sense in this example since we're using a 
    // singleton for the Dummy class (which I believe gets created automatically)
    // in the sencha app), but it would be necessary in other cases.
    initComponent: function (){
        this.items = [
            {
                html: SmallDemos.DummyText.mediumText
            },
            {
                title: 'Title',
                html: SmallDemos.DummyText.shortText
            },
            {
                title: 'Collapsible',
                collapsible: true,
                html: SmallDemos.DummyText.mediumText
            },
            {
                title: 'Tools',
                collapsible: true,
                collapsed: true,
                width: 640,
                html: SmallDemos.DummyText.extraLongText,
                tools:[
                    {type: 'pin'},
                    {type: 'refresh'},
                    {type: 'search'},
                    {type: 'save'}
                ],
                colspan: 3
            }
        ],
        this.callParent();
    }

/*
    items:[
        {
            html: SmallDemos.DummyText.mediumText
        },
        {
            title: 'Title',
            html: SmallDemos.DummyText.mediumText
        },
        {
            title: 'Collapsible',
            collapsible: true,
            html: SmallDemos.DummyText.mediumText
        },
        {
            title: 'Tools',
            collapsed: true,
            collapsible: true,
            width: 640,
            html: SmallDemos.DummyText.mediumText,
            tools: [
                {type: 'pin'},
                {type: 'refresh'},
                {type: 'search'},
                {type: 'save'}
            ],
            colspan: 3
        }
    ]
*/

});
